import java.util.Scanner;

public class E5_19 {
    public static void main(String[] args){
        System.out.print("Enter the card notation: ");
        Scanner inputObj = new Scanner(System.in);
        String strInput = inputObj.nextLine();
        Card cardInput = new Card(strInput);
        String strOutput = cardInput.getDescription();
        System.out.print(strOutput);

    }
}
