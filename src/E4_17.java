import java.util.Scanner;

public class E4_17 {
    public static void main(String[] args) {

        System.out.print("Please enter the first time: ");
        Scanner firstObj = new Scanner(System.in);
        String strFirstTime = firstObj.nextLine();

        System.out.print("Please enter the second time: ");
        Scanner secondObj = new Scanner(System.in);
        String strSecondTime = secondObj.nextLine();
        //https://www.w3schools.com/java/java_user_input.asp

        //Process data
        String[] strSplitFirstStrings = strFirstTime.split("");
        String strFirstMinute = strSplitFirstStrings[2]+strSplitFirstStrings[3];
        String strFirstHour = strSplitFirstStrings[0]+strSplitFirstStrings[1];
        int nFirstMinute = Integer.parseInt(strFirstMinute);
        int nFirstHour = Integer.parseInt(strFirstHour);

        String[] strSplitSecondStrings = strSecondTime.split("");
        String strSecondMinute = strSplitSecondStrings[2]+strSplitSecondStrings[3];
        String strSecondHour = strSplitSecondStrings[0]+strSplitSecondStrings[1];
        int nSecondMinute = Integer.parseInt(strSecondMinute);
        int nSecondHour = Integer.parseInt(strSecondHour);
        //https://www.javatpoint.com/java-string-to-int

        int nHours, nMinutes,nCarry=0;

        if (nSecondMinute<nFirstMinute){
            nMinutes = 60+nSecondMinute-nFirstMinute;
            nCarry=1;
        }
        else{
            nMinutes = nSecondMinute-nFirstMinute;
        }

        if (nSecondHour-nCarry<nFirstHour){
            nHours = 24+nSecondHour-nFirstHour-nCarry;
        }
        else{
            nHours = nSecondHour-nFirstHour-nCarry;
        }

        if (nMinutes==1 && nHours==1)
            System.out.println(nHours+" hour "+nMinutes+" minute");
        else if(nMinutes==1)
            System.out.println(nHours+" hours "+nMinutes+" minute");
        else if(nHours==1)
            System.out.println(nHours+" hour "+nMinutes+" minutes");
        else
            System.out.println(nHours+" hours "+nMinutes+" minutes");
    }
}