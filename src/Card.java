public class Card {
    String mValue;
    String mFace;
    int mLength;

    public Card(String strInputStrings){
        this.mLength = strInputStrings.length();
        String[] strFirstStrings = strInputStrings.split("");
        if (strFirstStrings[0].equals("1") && strFirstStrings[1].equals("0")){
            this.mFace = strFirstStrings[2];
            this.mValue = "10";
            mLength--;
        }
        else{
            this.mValue = strFirstStrings[0];
            this.mFace = strFirstStrings[1];
        }
    }

    String getType(String strInput){
        switch(strInput){
            case "A":
                return "Ace";
            case "2":
                return "Two";
            case "3":
                return "Three";
            case "4":
                return "Four";
            case "5":
                return "Five";
            case "6":
                return "Six";
            case "7":
                return "Seven";
            case "8":
                return "Eight";
            case "9":
                return "Nine";
            case "10":
                return "Ten";
            case "J":
                return "Jack";
            case "Q":
                return "Queen";
            case "K":
                return "King";
            case "D":
                return "Diamonds";
            case "H":
                return "Hearts";
            case "S":
                return "Spades";
            case "C":
                return "Clubs";
            default:
                return "U";
        }
    }

    String getDescription(){
        String strValueDescription = getType(mValue);
        String strFaceDescription = getType(mFace);
        if (mLength!=2 || strValueDescription.equals("U") || strFaceDescription.equals("U")){
            return("Unknown");
        }
        else
            return (strValueDescription+" of "+strFaceDescription);
    }
}
