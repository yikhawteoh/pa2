public class PrimeGenerator {
    int mNumber;
    public PrimeGenerator(int nInput){
        this.mNumber=nInput;
        String strOutput = nextPrime(this.mNumber);
        System.out.println(strOutput);
    }

    boolean isPrime(int nTestNum){
        boolean bFlag = true;
        if (nTestNum>1){
            for (int i=2;i<nTestNum;i++){
                if (nTestNum%i==0){
                    bFlag = false;
                    break;
                }
            }
        }
        return bFlag;
    }

    String nextPrime(int nTestNum){
        String strAnswer = "";
        for (int j=2; j<=nTestNum;j++){
            if (isPrime(j)) {
                strAnswer += Integer.toString(j);
                strAnswer += " ";
            }
        }
        return strAnswer;
    }

}
